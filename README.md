Interacting with the Twitter API (v2) using a client-server architecture on top of Node.js + Express.js.

- server.js - The server code that renders the webpages and handles the requests
- index.html - The index page that is served by the server at '/'
- client.js - The client code, loaded by the index.html page, that handles the async calls to the server and updates the page content whenever they are ready.

Inspired by https://gist.github.com/aerrity/fd393e5511106420fba0c9602cc05d35 but getting tweets and no DB.

