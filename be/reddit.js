import fetch from "node-fetch";
import { formatDate } from "./date.js"

/* Reddit constants */
const appId = "RhLvLjdgS7vJUz0jq7Kx-A";
const secret = "fADkR53BQDBr-nBmHEHDduV9TSRZAg"
const userAgent = "GetPosts/0.0.1"

var access_token = null;

export async function redditHandler(req, res) {
    console.log(`[redditHandler] server post called. Body: ${JSON.stringify(req.body)}`);

    const name = req.body.name;
    const [result, err] = await getUserComments(name)
    if (result.length == 0) {
        console.log(err)
        res.status(201).send(err)
    }
    else {
        res.send(result)
    }

};

function paramsWithLogin() {
    /*
    Login with 'grant_type' password, with username and password of random account
    Based on https://gist.github.com/davestevens/4257bbfc82b1e59eeec7085e66314215
    */
    const user = "mihai-bot"
    const password = "xwiQ6sUaLANy2KT"
    const params = new URLSearchParams();
    params.append("grant_type", "password");
    params.append("username", user);
    params.append("password", password);
    return params
}

function paramsWithoutLogin() {
    /* Auth without login. Use a random generated device id (google generate random uuid) */
    const deviceId = "ef75e058-ef0f-4768-80ef-ecdab4d31914" // random generated
    const params = new URLSearchParams();
    params.append("grant_type", "https://oauth.reddit.com/grants/installed_client");
    params.append("device_id", deviceId)
    return params
}

async function redditAuth() {
    const auth = "Basic " + Buffer.from(appId + ":" + secret).toString("base64")
    const params = paramsWithoutLogin()

    const options = {
        "method": "POST",
        "headers": {"Authorization": auth, "User-Agent": userAgent},
        "body": params
    }
    await fetch("https://www.reddit.com/api/v1/access_token", options)
        .then(response => response.json())
        .then((json) => {
            access_token = json["access_token"];
        })
}

async function getUserComments(user) {
    /* For a given user, return its previous 100 comments in an array of objects */
    var result = []
    var err = null

    await redditAuth(); /* TODO: user the same token unless it does't work anymore */
    console.log(`Bearer token: '${access_token}'`)

    const fetchParams = {
        "limit": 100
    }

    await fetch(`http://oauth.reddit.com/user/${user}/comments?` + new URLSearchParams(fetchParams), {
        method: "GET",
        headers: {
            "User-Agent": userAgent,
            "Authorization": `Bearer ${access_token}`
        }
    })
    .then(response => response.json())
    .then(json => {

        if (!("data" in json)) {
            err = json
            return
        }

        const comms = json["data"]["children"]
        for (var i = 0; i < comms.length; i++) {
            const currentComm = comms[i]
            const comment = currentComm.data.body
            const date = formatDate(new Date(currentComm.data.created_utc * 1000))
            const url = `<a href=https://www.reddit.com/${currentComm.data.permalink}> link </a>`
            result.push(
                {date: date, url: url, comment: comment}
            )
        }
    })
    return [result, err]
}
