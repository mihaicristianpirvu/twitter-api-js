/* Inspired by https://gist.github.com/aerrity/fd393e5511106420fba0c9602cc05d35 but getting tweets and no DB */

import express from "express"
import path from "path";
import bodyParser from "body-parser";

import { twitterHandler } from "./twitter.js"
import { redditHandler } from "./reddit.js"

/* Server stuff */
var app = express()
const feDir = path.resolve(path.dirname("")) + "/../fe"
app.use(express.static(feDir))
app.use(bodyParser.json())
console.log(feDir)

const port = process.argv.length == 3 ? parseInt(process.argv[2]) : 8080
var server = app.listen(port, function () {
   var host = server.address().address
   var port = server.address().port
   const hostAddr = host == "::" ? "localhost" : host

   console.log(`Example app listening at http://${hostAddr}:${port}`)
})

app.get("/", function (req, res) {
    res.sendFile(`${feDir}/index.html`);
})

app.post("/twitter", twitterHandler);

app.post("/reddit", redditHandler);
