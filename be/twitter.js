import fetch from "node-fetch";
import { formatDate } from "./date.js"

/* Twitter constants */
const bearerToken = "AAAAAAAAAAAAAAAAAAAAAHy0jQEAAAAAkcd4WMc7Fimk0NaracJvWxMOLls%3DBYFIZ34oxwhuO3OAfJMGSXa5kzqmRz0oGChAO7MfskO44JCHd9"
const options = {"headers": {"Authorization": `Bearer ${bearerToken}`}, "method": "GET"}

export async function twitterHandler(req, res) {
    console.log(`[twitter handler] server post called. Body: ${JSON.stringify(req.body)}`);
    const validSelects = ["name_and_tweets"]
    if (req.body == null || req.body.name == "" || !validSelects.includes(req.body.select)) {
        res.send("Got no body or invalid name or invalid select options");
        return;
    }

    const name = req.body.name;
    const [result, err] = await _twitterGetTweetsByUserName(name)
    if (result == null) {
        res.status(201).send(err)
    }
    else {
        res.send(result)
    }
};

async function _twitterGetIdByUserName(userName) {
    /* Simply gets the id and full name by the username */
    const userFields = "id"
    const response = await fetch(`https://api.twitter.com/2/users/by/username/${userName}?user.fields=${userFields}`,
                                 options);
    const data = await response.json();
    return data
}

async function _twitterGetTweetsByUserName(userName) {
    /* First, gets the id via twitterGetIdByUserName, then, based on the ID, gets the tweets.*/
    const response1 = await _twitterGetIdByUserName(userName);

    if (!("data" in response1)) {
        return [null, response1]
    }

    const id = response1["data"]["id"];
    const tweetFields = "id,author_id,text,created_at"
    const response2 = await fetch(`https://api.twitter.com/2/users/${id}/tweets?tweet.fields=${tweetFields}&max_results=20`,
                                  options)
    const data = await response2.json();

    const updatedResponse1 = {
        id: `<a href=https://www.twitter.com/${userName}> ${response1["data"]["id"]} </a>`,
        name: response1["data"]["name"],
        username: response1["data"]["username"]
    }

    var result = []
    if (data["data"] == null) {
        return {user: updatedResponse1, tweets: {data: "No tweets available for this user"}}
    }

    for(var i=0; i<data["data"].length; i++) {
        const currentResult = data["data"][i]
        const formatedDate = formatDate(new Date(currentResult["created_at"]))
        result.push({
            id: `<a href=https://www.twitter.com/${currentResult["author_id"]}/status/${currentResult["id"]}>${currentResult["id"]}</a>`,
            date: formatedDate,
            text: currentResult["text"]
        })
    }

    const res = {user: updatedResponse1, tweets: result}
    return [res, null]
}