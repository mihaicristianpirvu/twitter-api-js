/* Just javascript things. Not having basic functions. */

export function formatDate(date) {
    const day = ("0" + date.getDate()).slice(-2)
    const month = ("0" + (date.getMonth() + 1)).slice(-2)
    const hours = ("0" + (date.getHours() + 1)).slice(-2)
    const minutes = ("0" + (date.getMinutes() + 1)).slice(-2)
    const seconds = ("0" + (date.getSeconds() + 1)).slice(-2)
    const formatedDate = `${date.getFullYear()}.${month}.${day} ${hours}:${minutes}:${seconds}`
    return formatedDate
}
