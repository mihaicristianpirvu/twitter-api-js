/* THANK U NPM FOR MAKING ME WRITE THIS LOL */

export function jsonToTable(obj) {
    /*
    [
        {"h1": 1, "h2": 2, "_h3": 3},
        {"h1": 4, "h2": 5, "_h3": 6}
    ]

    =>
    <table>
        <th> <td>h1</td> <td>h2</td> <td>_h3</td> </th>
        <tr> <td>1</td> <td>2</td> <td>3</td> </tr>
        <tr> <td>1</td> <td>2</td> <td>3</td> </tr>
    </table>

    */
   if (!(obj instanceof Array)) {
       return jsonToTable([obj])
    }

    if (obj.length == 0) {
        throw new Error("Object has length 0")
    }

    var htmlTable = "<table>"
    const header = _getHeader(obj)
    const content = _getContent(obj)
    htmlTable = `<table> ${header} ${content} </table>`
    return htmlTable
 }

function _getContent(obj) {
    /* Gets the content of the table from a list of objets */
    var htmlTable = ""
    for (var i = 0; i < obj.length; i++) {
        var htmlTr = "<tr>"
        const items = Object.values(obj[i])
        for (var j = 0; j < items.length; j++) {
            htmlTr += `<td> ${items[j]} </td>`
        }
        htmlTr += "</tr>"
        htmlTable += htmlTr;
    }
    return htmlTable
}

function _getHeader(obj) {
    /* gets the header from the list of objects. All objects must have the same header, otherwise error. */
    const firstHeader = Object.keys(obj[0])
    for (var i = 1; i < obj.length; i++) {
        var header = Object.keys(obj[i]);
        if (header.toString() != firstHeader.toString()) {
            throw new Error(`Header incompatibility: ${firstHeader} vs ${header}`)
        }
    }

    var htmlTh = "<tr>";
    for (var i = 0; i < firstHeader.length; i++){
        htmlTh += `<th> ${firstHeader[i]} </th>`
    }
    htmlTh = `${htmlTh} </tr>`
    return htmlTh
}
