import { jsonToTable } from "./json_to_table.js"

console.log("Client-side code running");
const inButton = document.getElementById("inButton");
const inputText = document.getElementById("inInput");
const inSelect = document.getElementById("inSelect");
const outDiv = document.getElementById("outDiv");

/**
 * Generic prefix function. This allows us to use both locahost:port and some_host/blog/ with same api calls
 * @return The prefix (/blog) based on the browser's href.
 */
function getPrefix() {
    const url = new URL(window.location.href)
    if (url.port != "") {
        return ""
    }
    const splitPathname = url.pathname.split("/")
    return `/${splitPathname[1]}`
}

function buttonClickFn() {
  if (inputText.value == ""){
    outDiv.innerHTML = "No username was provided";
    throw new Error("No username provided");
  }

  console.log(`Front-end select: ${inSelect.value}`);
  /* In data is taken from inputText and inSelect */
  const inData = {name: inputText.value, select: inSelect.value}
  const options = {
    method: "POST",
    body: JSON.stringify(inData),
    headers: {"Content-Type": "application/json"}
  }

  var fetchUrl = null;
  const prefix = getPrefix()
  if (inSelect.value == "name_and_tweets") {
    fetchUrl = `${prefix}/twitter`
  }
  else if (inSelect.value == "reddit_posts") {
    fetchUrl = `${prefix}/reddit`
  }

  return [fetchUrl, options]
}

function handleResult(response, data) {
  /* Function that handles the results coming from the server */
  console.log(`Got response ${JSON.stringify(data)}`);
  if (response.status == 200) {
    if(inSelect.value == "name_and_tweets"){
      outDiv.innerHTML = jsonToTable(data["user"])
      outDiv.innerHTML += jsonToTable(data["tweets"])
    }
    else{
      outDiv.innerHTML = jsonToTable(data)
    }
  }
  else {
    outDiv.innerHTML = JSON.stringify(data)
  };
}

(function() {
  /* Main function of this code. */
  inputText.focus();

  /* Enter when selecting the input will press the button */
  document.addEventListener("keypress", function(e) {
    if(e.key == "Enter") {
      e.preventDefault();
      inButton.click();
    }
  });

  /* When pressing 'click', send the data to the server. */
  inButton.addEventListener("click", (e) => {
    console.log("button was clicked");
    const [fetchUrl, options] = buttonClickFn();

    fetch(fetchUrl, options)
      .then((response) => {
        if(!response.ok) {
          throw new Error("Request failed.")
        }

        console.log("Click was recorded");
        response.json().then((data) => handleResult(response, data));
      })
      .catch((error) => {
        console.log(error);
      });
  })

})()
